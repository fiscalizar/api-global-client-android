
# Votes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**elections** | [**List&lt;VotesElections&gt;**](VotesElections.md) |  |  [optional]
**regions** | [**List&lt;VotesRegions&gt;**](VotesRegions.md) |  |  [optional]
**sections** | [**List&lt;VotesSections&gt;**](VotesSections.md) |  |  [optional]



