
# AccessByCircuit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idAccessGranted** | [**AccessesGranted**](AccessesGranted.md) |  |  [optional]
**idCircuit** | [**Circuits**](Circuits.md) |  |  [optional]



