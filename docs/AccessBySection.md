
# AccessBySection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idAccessGranted** | [**AccessesGranted**](AccessesGranted.md) |  |  [optional]
**idSection** | [**Sections**](Sections.md) |  |  [optional]



