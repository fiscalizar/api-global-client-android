
# VotesCircuits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**circuit** | [**Circuits**](Circuits.md) |  |  [optional]
**categories** | [**List&lt;VotesCategories&gt;**](VotesCategories.md) |  |  [optional]
**institutions** | [**List&lt;VotesInstitutions&gt;**](VotesInstitutions.md) |  |  [optional]
**votedTables** | **Long** |  |  [optional]
**percentageByVotedTables** | **Integer** |  |  [optional]
**votedElectors** | **Long** |  |  [optional]
**percentageByVotedElectors** | **Integer** |  |  [optional]



