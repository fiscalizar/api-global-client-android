
# Categories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**elections** | [**List&lt;Elections&gt;**](Elections.md) |  |  [optional]
**parties** | [**List&lt;Parties&gt;**](Parties.md) |  |  [optional]
**polls** | [**List&lt;Polls&gt;**](Polls.md) |  |  [optional]



