
# VotesElections

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**election** | [**Elections**](Elections.md) |  |  [optional]
**categories** | [**List&lt;VotesCategories&gt;**](VotesCategories.md) |  |  [optional]
**regions** | [**List&lt;VotesRegions&gt;**](VotesRegions.md) |  |  [optional]
**sections** | [**List&lt;VotesSections&gt;**](VotesSections.md) |  |  [optional]
**circuits** | [**List&lt;VotesCircuits&gt;**](VotesCircuits.md) |  |  [optional]
**institutions** | [**List&lt;VotesInstitutions&gt;**](VotesInstitutions.md) |  |  [optional]
**votingTables** | [**List&lt;VotesVotingTables&gt;**](VotesVotingTables.md) |  |  [optional]
**votedTables** | **Long** |  |  [optional]
**percentageByVotedTables** | **Integer** |  |  [optional]
**votedElectors** | **Long** |  |  [optional]
**percentageByVotedElectors** | **Integer** |  |  [optional]



