
# PollsFiles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**idPoll** | [**Polls**](Polls.md) |  |  [optional]
**file** | **String** |  |  [optional]
**checkSum** | **String** |  |  [optional]



