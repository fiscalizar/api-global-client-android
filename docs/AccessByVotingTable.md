
# AccessByVotingTable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idAccessGranted** | [**AccessesGranted**](AccessesGranted.md) |  |  [optional]
**idVotingTable** | [**VotingTables**](VotingTables.md) |  |  [optional]



