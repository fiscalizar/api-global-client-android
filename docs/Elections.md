
# Elections

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**year** | **String** |  |  [optional]
**parties** | [**List&lt;Parties&gt;**](Parties.md) |  |  [optional]
**categories** | [**List&lt;Categories&gt;**](Categories.md) |  |  [optional]
**regions** | [**List&lt;Regions&gt;**](Regions.md) |  |  [optional]
**sections** | [**List&lt;Sections&gt;**](Sections.md) |  |  [optional]
**polls** | [**List&lt;Polls&gt;**](Polls.md) |  |  [optional]
**accessesGranted** | [**List&lt;AccessesGranted&gt;**](AccessesGranted.md) |  |  [optional]



