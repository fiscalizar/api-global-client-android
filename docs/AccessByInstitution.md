
# AccessByInstitution

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idAccessGranted** | [**AccessesGranted**](AccessesGranted.md) |  |  [optional]
**idInstitution** | [**Institutions**](Institutions.md) |  |  [optional]



