
# VotesParties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**party** | [**Parties**](Parties.md) |  |  [optional]
**votes** | **Long** |  |  [optional]
**percentage** | **Integer** |  |  [optional]



