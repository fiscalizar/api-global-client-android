
# VotesInstitutions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**institution** | [**Institutions**](Institutions.md) |  |  [optional]
**categories** | [**List&lt;VotesCategories&gt;**](VotesCategories.md) |  |  [optional]
**votingTables** | [**List&lt;VotesVotingTables&gt;**](VotesVotingTables.md) |  |  [optional]
**votedTables** | **Long** |  |  [optional]
**percentageByVotedTables** | **Integer** |  |  [optional]
**votedElectors** | **Long** |  |  [optional]
**percentageByVotedElectors** | **Integer** |  |  [optional]



