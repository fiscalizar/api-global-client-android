
# Users

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**identification** | **String** |  |  [optional]
**role** | [**UsersRole**](UsersRole.md) |  |  [optional]
**birth** | [**Date**](Date.md) |  |  [optional]
**password** | **String** |  |  [optional]
**subscribePush** | **String** |  |  [optional]
**subscribeEmail** | **String** |  |  [optional]
**externalId** | **String** |  |  [optional]
**facebookId** | **String** |  |  [optional]
**twitterId** | **String** |  |  [optional]
**googleId** | **String** |  |  [optional]
**emailValid** | **Boolean** |  |  [optional]
**passwordReset** | **Boolean** |  |  [optional]
**memberships** | [**List&lt;Memberships&gt;**](Memberships.md) |  |  [optional]



