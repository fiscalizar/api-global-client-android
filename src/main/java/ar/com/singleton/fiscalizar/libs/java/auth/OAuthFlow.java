/*
 * FiscalizAR - Global API
 * FiscalizAR Global API to interact with main database headquarters.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: soporte@singleton.com.ar
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package ar.com.singleton.fiscalizar.libs.java.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}
