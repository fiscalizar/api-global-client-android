
# Accounts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**memberships** | [**List&lt;Memberships&gt;**](Memberships.md) |  |  [optional]



