# StadisticsApi

All URIs are relative to *http://localhost:3000/backend/api/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStadistics**](StadisticsApi.md#getStadistics) | **GET** /stadistics | Get Stadistics.


<a name="getStadistics"></a>
# **getStadistics**
> Object getStadistics(skip, limit, orderBy, idVotingTable, idInstitution, idCircuit, idSection, idRegion, idElection, deleted, metadata)

Get Stadistics.

Get Stadistics.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.StadisticsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

StadisticsApi apiInstance = new StadisticsApi();
Integer skip = 56; // Integer | number of item to skip
Integer limit = 56; // Integer | max records to return
String orderBy = "orderBy_example"; // String | order by property.
Long idVotingTable = 789L; // Long | idVotingTable to delete or search
Long idInstitution = 789L; // Long | idInstitution to delete or search
Long idCircuit = 789L; // Long | idCircuit to delete or search
Long idSection = 789L; // Long | idSection to delete or search
Long idRegion = 789L; // Long | idRegion to delete or search
Long idElection = 789L; // Long | idElection to delete or search
String deleted = "NOT-DELETED"; // String | Get all, deleted, not deleted data. Default not deleted.
Boolean metadata = true; // Boolean | If metadata is needed (for pagination controls)
try {
    Object result = apiInstance.getStadistics(skip, limit, orderBy, idVotingTable, idInstitution, idCircuit, idSection, idRegion, idElection, deleted, metadata);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StadisticsApi#getStadistics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **Integer**| number of item to skip |
 **limit** | **Integer**| max records to return |
 **orderBy** | **String**| order by property. | [optional]
 **idVotingTable** | **Long**| idVotingTable to delete or search | [optional]
 **idInstitution** | **Long**| idInstitution to delete or search | [optional]
 **idCircuit** | **Long**| idCircuit to delete or search | [optional]
 **idSection** | **Long**| idSection to delete or search | [optional]
 **idRegion** | **Long**| idRegion to delete or search | [optional]
 **idElection** | **Long**| idElection to delete or search | [optional]
 **deleted** | **String**| Get all, deleted, not deleted data. Default not deleted. | [optional] [default to NOT-DELETED] [enum: NOT-DELETED, DELETED, ALL]
 **metadata** | **Boolean**| If metadata is needed (for pagination controls) | [optional]

### Return type

**Object**

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

