
# Parties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**code** | **String** |  |  [optional]
**elections** | [**List&lt;Elections&gt;**](Elections.md) |  |  [optional]
**categories** | [**List&lt;Categories&gt;**](Categories.md) |  |  [optional]
**polls** | [**List&lt;Polls&gt;**](Polls.md) |  |  [optional]



