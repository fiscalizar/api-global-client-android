
# VotesVotingTables

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**votingTable** | [**VotingTables**](VotingTables.md) |  |  [optional]
**categories** | [**List&lt;VotesCategories&gt;**](VotesCategories.md) |  |  [optional]
**votedElectors** | **Long** |  |  [optional]



