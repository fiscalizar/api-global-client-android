# UsersApi

All URIs are relative to *http://localhost:3000/backend/api/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addUser**](UsersApi.md#addUser) | **POST** /users | Add one User.
[**deleteUsers**](UsersApi.md#deleteUsers) | **DELETE** /users/{id} | Delete one Users.
[**editUser**](UsersApi.md#editUser) | **PUT** /users | Edit one User.
[**getUserById**](UsersApi.md#getUserById) | **GET** /users/{id} | Get one User.
[**getUsers**](UsersApi.md#getUsers) | **GET** /users | Get all Users.
[**login**](UsersApi.md#login) | **GET** /users/login | Logs user into the system
[**loginToken**](UsersApi.md#loginToken) | **GET** /users/login/{token} | Logs user into the system by providing token
[**logout**](UsersApi.md#logout) | **GET** /users/logout | Logs out current logged in user session
[**verifyToken**](UsersApi.md#verifyToken) | **GET** /users/verifyToken | Get user info by token


<a name="addUser"></a>
# **addUser**
> Users addUser(user)

Add one User.

Add one User.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

UsersApi apiInstance = new UsersApi();
Users user = new Users(); // Users | 
try {
    Users result = apiInstance.addUser(user);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#addUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**Users**](Users.md)|  |

### Return type

[**Users**](Users.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="deleteUsers"></a>
# **deleteUsers**
> DeletedResponse deleteUsers(id)

Delete one Users.

Delete one User.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

UsersApi apiInstance = new UsersApi();
Long id = 789L; // Long | id to delete or search
try {
    DeletedResponse result = apiInstance.deleteUsers(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#deleteUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id to delete or search |

### Return type

[**DeletedResponse**](DeletedResponse.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="editUser"></a>
# **editUser**
> Users editUser(user)

Edit one User.

Edit one User.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

UsersApi apiInstance = new UsersApi();
Users user = new Users(); // Users | 
try {
    Users result = apiInstance.editUser(user);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#editUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**Users**](Users.md)|  |

### Return type

[**Users**](Users.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="getUserById"></a>
# **getUserById**
> Users getUserById(id, deleted)

Get one User.

Get one User.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

UsersApi apiInstance = new UsersApi();
Long id = 789L; // Long | id to delete or search
String deleted = "NOT-DELETED"; // String | Get all, deleted, not deleted data. Default not deleted.
try {
    Users result = apiInstance.getUserById(id, deleted);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getUserById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id to delete or search |
 **deleted** | **String**| Get all, deleted, not deleted data. Default not deleted. | [optional] [default to NOT-DELETED] [enum: NOT-DELETED, DELETED, ALL]

### Return type

[**Users**](Users.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="getUsers"></a>
# **getUsers**
> Object getUsers(skip, limit, orderBy, filter, deleted, metadata)

Get all Users.

Get all Users.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

UsersApi apiInstance = new UsersApi();
Integer skip = 56; // Integer | number of item to skip
Integer limit = 56; // Integer | max records to return
String orderBy = "orderBy_example"; // String | order by property.
String filter = "filter_example"; // String | filter data.
String deleted = "NOT-DELETED"; // String | Get all, deleted, not deleted data. Default not deleted.
Boolean metadata = true; // Boolean | If metadata is needed (for pagination controls)
try {
    Object result = apiInstance.getUsers(skip, limit, orderBy, filter, deleted, metadata);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#getUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **Integer**| number of item to skip |
 **limit** | **Integer**| max records to return |
 **orderBy** | **String**| order by property. | [optional]
 **filter** | **String**| filter data. | [optional]
 **deleted** | **String**| Get all, deleted, not deleted data. Default not deleted. | [optional] [default to NOT-DELETED] [enum: NOT-DELETED, DELETED, ALL]
 **metadata** | **Boolean**| If metadata is needed (for pagination controls) | [optional]

### Return type

**Object**

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="login"></a>
# **login**
> LoginObject login(email, password)

Logs user into the system

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

UsersApi apiInstance = new UsersApi();
String email = "email_example"; // String | The user email for login
String password = "password_example"; // String | The password for login in clear text
try {
    LoginObject result = apiInstance.login(email, password);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#login");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| The user email for login |
 **password** | **String**| The password for login in clear text |

### Return type

[**LoginObject**](LoginObject.md)

### Authorization

[appToken](../README.md#appToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="loginToken"></a>
# **loginToken**
> LoginObject loginToken(token)

Logs user into the system by providing token

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

UsersApi apiInstance = new UsersApi();
String token = "token_example"; // String | The access token
try {
    LoginObject result = apiInstance.loginToken(token);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#loginToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**| The access token |

### Return type

[**LoginObject**](LoginObject.md)

### Authorization

[appToken](../README.md#appToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="logout"></a>
# **logout**
> String logout()

Logs out current logged in user session

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;


UsersApi apiInstance = new UsersApi();
try {
    String result = apiInstance.logout();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#logout");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="verifyToken"></a>
# **verifyToken**
> LoginObject verifyToken()

Get user info by token

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.UsersApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

UsersApi apiInstance = new UsersApi();
try {
    LoginObject result = apiInstance.verifyToken();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UsersApi#verifyToken");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LoginObject**](LoginObject.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

