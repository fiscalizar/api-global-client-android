# PasswordsApi

All URIs are relative to *http://localhost:3000/backend/api/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changePassword**](PasswordsApi.md#changePassword) | **GET** /users/password/change | Change user password by providing old one.
[**changePasswordWithToken**](PasswordsApi.md#changePasswordWithToken) | **GET** /users/password/change/{token} | Change user password with a token.
[**requestPassword**](PasswordsApi.md#requestPassword) | **GET** /users/password/forgot | Request a change of password by email.


<a name="changePassword"></a>
# **changePassword**
> changePassword(oldPassword, newPassword, userId)

Change user password by providing old one.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.PasswordsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

PasswordsApi apiInstance = new PasswordsApi();
String oldPassword = "oldPassword_example"; // String | Old Password
String newPassword = "newPassword_example"; // String | New Password
String userId = "userId_example"; // String | User to change password. Only for admin users.
try {
    apiInstance.changePassword(oldPassword, newPassword, userId);
} catch (ApiException e) {
    System.err.println("Exception when calling PasswordsApi#changePassword");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **oldPassword** | **String**| Old Password |
 **newPassword** | **String**| New Password |
 **userId** | **String**| User to change password. Only for admin users. | [optional]

### Return type

null (empty response body)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="changePasswordWithToken"></a>
# **changePasswordWithToken**
> changePasswordWithToken(token, password)

Change user password with a token.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.PasswordsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

PasswordsApi apiInstance = new PasswordsApi();
String token = "token_example"; // String | The access token
String password = "password_example"; // String | The access token
try {
    apiInstance.changePasswordWithToken(token, password);
} catch (ApiException e) {
    System.err.println("Exception when calling PasswordsApi#changePasswordWithToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**| The access token |
 **password** | **String**| The access token |

### Return type

null (empty response body)

### Authorization

[appToken](../README.md#appToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="requestPassword"></a>
# **requestPassword**
> requestPassword(email)

Request a change of password by email.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.PasswordsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

PasswordsApi apiInstance = new PasswordsApi();
String email = "email_example"; // String | User's email.
try {
    apiInstance.requestPassword(email);
} catch (ApiException e) {
    System.err.println("Exception when calling PasswordsApi#requestPassword");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| User&#39;s email. |

### Return type

null (empty response body)

### Authorization

[appToken](../README.md#appToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

