
# AccessByRegion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idAccessGranted** | [**AccessesGranted**](AccessesGranted.md) |  |  [optional]
**idRegion** | [**Regions**](Regions.md) |  |  [optional]



