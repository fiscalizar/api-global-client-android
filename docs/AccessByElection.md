
# AccessByElection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**idAccessGranted** | [**AccessesGranted**](AccessesGranted.md) |  |  [optional]
**idElection** | [**Elections**](Elections.md) |  |  [optional]



