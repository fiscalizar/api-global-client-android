
# VotingTables

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**code** | **String** |  |  [optional]
**electors** | **Long** |  |  [optional]
**idInstitution** | [**Institutions**](Institutions.md) |  |  [optional]
**polls** | [**List&lt;Polls&gt;**](Polls.md) |  |  [optional]
**accessesGranted** | [**List&lt;AccessesGranted&gt;**](AccessesGranted.md) |  |  [optional]



