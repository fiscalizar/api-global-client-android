
# Sections

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**idRegion** | [**Regions**](Regions.md) |  |  [optional]
**circuits** | [**List&lt;Circuits&gt;**](Circuits.md) |  |  [optional]
**elections** | [**List&lt;Elections&gt;**](Elections.md) |  |  [optional]
**accessesGranted** | [**List&lt;AccessesGranted&gt;**](AccessesGranted.md) |  |  [optional]



