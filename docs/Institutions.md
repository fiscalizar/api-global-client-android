
# Institutions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**address** | **String** |  |  [optional]
**locationLat** | **String** |  |  [optional]
**locationLong** | **String** |  |  [optional]
**idCircuit** | [**Circuits**](Circuits.md) |  |  [optional]
**votingTables** | [**List&lt;VotingTables&gt;**](VotingTables.md) |  |  [optional]
**accessesGranted** | [**List&lt;AccessesGranted&gt;**](AccessesGranted.md) |  |  [optional]



