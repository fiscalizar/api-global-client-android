
# Regions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**elections** | [**List&lt;Elections&gt;**](Elections.md) |  |  [optional]
**sections** | [**List&lt;Sections&gt;**](Sections.md) |  |  [optional]
**accessesGranted** | [**List&lt;AccessesGranted&gt;**](AccessesGranted.md) |  |  [optional]



