
# Polls

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**idCategory** | [**Categories**](Categories.md) |  |  [optional]
**idParty** | [**Parties**](Parties.md) |  |  [optional]
**idVotingTable** | [**VotingTables**](VotingTables.md) |  |  [optional]
**idElection** | [**Elections**](Elections.md) |  |  [optional]
**files** | [**List&lt;PollsFiles&gt;**](PollsFiles.md) |  |  [optional]
**accessesGranted** | [**List&lt;AccessesGranted&gt;**](AccessesGranted.md) |  |  [optional]



