/*
 * FiscalizAR - Global API
 * FiscalizAR Global API to interact with main database headquarters.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: soporte@singleton.com.ar
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package ar.com.singleton.fiscalizar.libs.java.api;

import ar.com.singleton.fiscalizar.libs.java.ApiException;
import ar.com.singleton.fiscalizar.libs.java.model.CustomError;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for PasswordsApi
 */
@Ignore
public class PasswordsApiTest {

    private final PasswordsApi api = new PasswordsApi();

    
    /**
     * Change user password by providing old one.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void changePasswordTest() throws ApiException {
        String oldPassword = null;
        String newPassword = null;
        String userId = null;
        api.changePassword(oldPassword, newPassword, userId);

        // TODO: test validations
    }
    
    /**
     * Change user password with a token.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void changePasswordWithTokenTest() throws ApiException {
        String token = null;
        String password = null;
        api.changePasswordWithToken(token, password);

        // TODO: test validations
    }
    
    /**
     * Request a change of password by email.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void requestPasswordTest() throws ApiException {
        String email = null;
        api.requestPassword(email);

        // TODO: test validations
    }
    
}
