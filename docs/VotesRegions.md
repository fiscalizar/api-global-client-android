
# VotesRegions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**region** | [**Regions**](Regions.md) |  |  [optional]
**categories** | [**List&lt;VotesCategories&gt;**](VotesCategories.md) |  |  [optional]
**sections** | [**List&lt;VotesSections&gt;**](VotesSections.md) |  |  [optional]
**votedTables** | **Long** |  |  [optional]
**percentageByVotedTables** | **Integer** |  |  [optional]
**votedElectors** | **Long** |  |  [optional]
**percentageByVotedElectors** | **Integer** |  |  [optional]



