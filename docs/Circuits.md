
# Circuits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**code** | **String** |  |  [optional]
**idSection** | [**Sections**](Sections.md) |  |  [optional]
**institutions** | [**List&lt;Institutions&gt;**](Institutions.md) |  |  [optional]
**accessesGranted** | [**List&lt;AccessesGranted&gt;**](AccessesGranted.md) |  |  [optional]



