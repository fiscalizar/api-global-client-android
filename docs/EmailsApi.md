# EmailsApi

All URIs are relative to *http://localhost:3000/backend/api/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sendEmail**](EmailsApi.md#sendEmail) | **POST** /users/email/request | send verification email to user
[**verifyEmail**](EmailsApi.md#verifyEmail) | **GET** /users/email/verify/{token} | Verify user&#39;s email


<a name="sendEmail"></a>
# **sendEmail**
> sendEmail(data)

send verification email to user

Send verification email to user

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.EmailsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

EmailsApi apiInstance = new EmailsApi();
Data data = new Data(); // Data | 
try {
    apiInstance.sendEmail(data);
} catch (ApiException e) {
    System.err.println("Exception when calling EmailsApi#sendEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Data**](Data.md)|  |

### Return type

null (empty response body)

### Authorization

[appToken](../README.md#appToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="verifyEmail"></a>
# **verifyEmail**
> LoginObject verifyEmail(token)

Verify user&#39;s email

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.EmailsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

EmailsApi apiInstance = new EmailsApi();
String token = "token_example"; // String | Access token
try {
    LoginObject result = apiInstance.verifyEmail(token);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EmailsApi#verifyEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**| Access token |

### Return type

[**LoginObject**](LoginObject.md)

### Authorization

[appToken](../README.md#appToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

