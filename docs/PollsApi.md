# PollsApi

All URIs are relative to *http://localhost:3000/backend/api/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addPoll**](PollsApi.md#addPoll) | **POST** /polls | Add one Poll.
[**deletePoll**](PollsApi.md#deletePoll) | **DELETE** /polls/{id} | Delete one Poll.
[**editPoll**](PollsApi.md#editPoll) | **PUT** /polls | Edit one Poll.
[**getPollById**](PollsApi.md#getPollById) | **GET** /polls/{id} | Get one Poll.
[**getPolls**](PollsApi.md#getPolls) | **GET** /polls | Get all Polls.


<a name="addPoll"></a>
# **addPoll**
> Polls addPoll(poll)

Add one Poll.

Add one Poll.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
Polls poll = new Polls(); // Polls | 
try {
    Polls result = apiInstance.addPoll(poll);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#addPoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poll** | [**Polls**](Polls.md)|  |

### Return type

[**Polls**](Polls.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="deletePoll"></a>
# **deletePoll**
> DeletedResponse deletePoll(id)

Delete one Poll.

Delete one Poll.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
Long id = 789L; // Long | id to delete or search
try {
    DeletedResponse result = apiInstance.deletePoll(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#deletePoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id to delete or search |

### Return type

[**DeletedResponse**](DeletedResponse.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="editPoll"></a>
# **editPoll**
> Polls editPoll(poll)

Edit one Poll.

Edit one Poll.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
Polls poll = new Polls(); // Polls | 
try {
    Polls result = apiInstance.editPoll(poll);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#editPoll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poll** | [**Polls**](Polls.md)|  |

### Return type

[**Polls**](Polls.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="getPollById"></a>
# **getPollById**
> Polls getPollById(id, deleted)

Get one Poll.

Get one Poll.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
Long id = 789L; // Long | id to delete or search
String deleted = "NOT-DELETED"; // String | Get all, deleted, not deleted data. Default not deleted.
try {
    Polls result = apiInstance.getPollById(id, deleted);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getPollById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Long**| id to delete or search |
 **deleted** | **String**| Get all, deleted, not deleted data. Default not deleted. | [optional] [default to NOT-DELETED] [enum: NOT-DELETED, DELETED, ALL]

### Return type

[**Polls**](Polls.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="getPolls"></a>
# **getPolls**
> Object getPolls(skip, limit, orderBy, filter, deleted, metadata)

Get all Polls.

Get all Polls.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.PollsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

PollsApi apiInstance = new PollsApi();
Integer skip = 56; // Integer | number of item to skip
Integer limit = 56; // Integer | max records to return
String orderBy = "orderBy_example"; // String | order by property.
String filter = "filter_example"; // String | filter data.
String deleted = "NOT-DELETED"; // String | Get all, deleted, not deleted data. Default not deleted.
Boolean metadata = true; // Boolean | If metadata is needed (for pagination controls)
try {
    Object result = apiInstance.getPolls(skip, limit, orderBy, filter, deleted, metadata);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PollsApi#getPolls");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **Integer**| number of item to skip |
 **limit** | **Integer**| max records to return |
 **orderBy** | **String**| order by property. | [optional]
 **filter** | **String**| filter data. | [optional]
 **deleted** | **String**| Get all, deleted, not deleted data. Default not deleted. | [optional] [default to NOT-DELETED] [enum: NOT-DELETED, DELETED, ALL]
 **metadata** | **Boolean**| If metadata is needed (for pagination controls) | [optional]

### Return type

**Object**

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

