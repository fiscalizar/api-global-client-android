
# AccessBy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**elections** | [**List&lt;AccessByElection&gt;**](AccessByElection.md) |  |  [optional]
**regions** | [**List&lt;AccessByRegion&gt;**](AccessByRegion.md) |  |  [optional]
**sections** | [**List&lt;AccessBySection&gt;**](AccessBySection.md) |  |  [optional]
**circuits** | [**List&lt;AccessByCircuit&gt;**](AccessByCircuit.md) |  |  [optional]
**institutuions** | [**List&lt;AccessByInstitution&gt;**](AccessByInstitution.md) |  |  [optional]
**votingTables** | [**List&lt;AccessByVotingTable&gt;**](AccessByVotingTable.md) |  |  [optional]



