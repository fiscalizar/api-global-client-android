/*
 * FiscalizAR - Global API
 * FiscalizAR Global API to interact with main database headquarters.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: soporte@singleton.com.ar
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package ar.com.singleton.fiscalizar.libs.java.model;

import java.util.Objects;
import java.util.Arrays;
import ar.com.singleton.fiscalizar.libs.java.model.AccessesGranted;
import ar.com.singleton.fiscalizar.libs.java.model.Sections;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.io.Serializable;

/**
 * AccessBySection
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-09T18:03:23.877+02:00")
public class AccessBySection implements Serializable {
  private static final long serialVersionUID = 1L;

  @SerializedName("idAccessGranted")
  private AccessesGranted idAccessGranted = null;

  @SerializedName("idSection")
  private Sections idSection = null;

  public AccessBySection idAccessGranted(AccessesGranted idAccessGranted) {
    this.idAccessGranted = idAccessGranted;
    return this;
  }

   /**
   * Get idAccessGranted
   * @return idAccessGranted
  **/
  @ApiModelProperty(value = "")
  public AccessesGranted getIdAccessGranted() {
    return idAccessGranted;
  }

  public void setIdAccessGranted(AccessesGranted idAccessGranted) {
    this.idAccessGranted = idAccessGranted;
  }

  public AccessBySection idSection(Sections idSection) {
    this.idSection = idSection;
    return this;
  }

   /**
   * Get idSection
   * @return idSection
  **/
  @ApiModelProperty(value = "")
  public Sections getIdSection() {
    return idSection;
  }

  public void setIdSection(Sections idSection) {
    this.idSection = idSection;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccessBySection accessBySection = (AccessBySection) o;
    return Objects.equals(this.idAccessGranted, accessBySection.idAccessGranted) &&
        Objects.equals(this.idSection, accessBySection.idSection);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idAccessGranted, idSection);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccessBySection {\n");
    
    sb.append("    idAccessGranted: ").append(toIndentedString(idAccessGranted)).append("\n");
    sb.append("    idSection: ").append(toIndentedString(idSection)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

