# AccessByApi

All URIs are relative to *http://localhost:3000/backend/api/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAccessBy**](AccessByApi.md#addAccessBy) | **POST** /accessBy | Add one AccessBy.
[**deleteAccessBy**](AccessByApi.md#deleteAccessBy) | **DELETE** /accessBy/{idVotingTable}/{idInstitution}/{idCircuit}/{idSection}/{idRegion}/{idElection}/{idUser} | Delete one AccessBy.
[**editAccessBy**](AccessByApi.md#editAccessBy) | **PUT** /accessBy | Edit one AccessBy.
[**getAccessBy**](AccessByApi.md#getAccessBy) | **GET** /accessBy | Get all AccessBy.
[**getAccessByById**](AccessByApi.md#getAccessByById) | **GET** /accessBy/{idVotingTable}/{idInstitution}/{idCircuit}/{idSection}/{idRegion}/{idElection}/{idUser} | Get one AccessBy.


<a name="addAccessBy"></a>
# **addAccessBy**
> AccessBy addAccessBy(accessBy)

Add one AccessBy.

Add one AccessBy.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.AccessByApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

AccessByApi apiInstance = new AccessByApi();
AccessBy accessBy = new AccessBy(); // AccessBy | 
try {
    AccessBy result = apiInstance.addAccessBy(accessBy);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccessByApi#addAccessBy");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessBy** | [**AccessBy**](AccessBy.md)|  |

### Return type

[**AccessBy**](AccessBy.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="deleteAccessBy"></a>
# **deleteAccessBy**
> DeletedResponse deleteAccessBy(idUser, idVotingTable, idInstitution, idCircuit, idSection, idRegion, idElection)

Delete one AccessBy.

Delete one AccessBy.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.AccessByApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

AccessByApi apiInstance = new AccessByApi();
Long idUser = 789L; // Long | idUser to delete or search.
Long idVotingTable = 789L; // Long | idVotingTable to delete or search
Long idInstitution = 789L; // Long | idInstitution to delete or search
Long idCircuit = 789L; // Long | idCircuit to delete or search
Long idSection = 789L; // Long | idSection to delete or search
Long idRegion = 789L; // Long | idRegion to delete or search
Long idElection = 789L; // Long | idElection to delete or search
try {
    DeletedResponse result = apiInstance.deleteAccessBy(idUser, idVotingTable, idInstitution, idCircuit, idSection, idRegion, idElection);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccessByApi#deleteAccessBy");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idUser** | **Long**| idUser to delete or search. |
 **idVotingTable** | **Long**| idVotingTable to delete or search |
 **idInstitution** | **Long**| idInstitution to delete or search |
 **idCircuit** | **Long**| idCircuit to delete or search |
 **idSection** | **Long**| idSection to delete or search |
 **idRegion** | **Long**| idRegion to delete or search |
 **idElection** | **Long**| idElection to delete or search |

### Return type

[**DeletedResponse**](DeletedResponse.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="editAccessBy"></a>
# **editAccessBy**
> AccessBy editAccessBy(accessBy)

Edit one AccessBy.

Edit one AccessBy.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.AccessByApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

AccessByApi apiInstance = new AccessByApi();
AccessBy accessBy = new AccessBy(); // AccessBy | 
try {
    AccessBy result = apiInstance.editAccessBy(accessBy);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccessByApi#editAccessBy");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessBy** | [**AccessBy**](AccessBy.md)|  |

### Return type

[**AccessBy**](AccessBy.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="getAccessBy"></a>
# **getAccessBy**
> Object getAccessBy(skip, limit, orderBy, idUser, idVotingTable, idInstitution, idCircuit, idSection, idRegion, idElection, deleted, metadata)

Get all AccessBy.

Get all AccessBy.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.AccessByApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

AccessByApi apiInstance = new AccessByApi();
Integer skip = 56; // Integer | number of item to skip
Integer limit = 56; // Integer | max records to return
String orderBy = "orderBy_example"; // String | order by property.
Long idUser = 789L; // Long | id of user to get information only from it
Long idVotingTable = 789L; // Long | idVotingTable to delete or search
Long idInstitution = 789L; // Long | idInstitution to delete or search
Long idCircuit = 789L; // Long | idCircuit to delete or search
Long idSection = 789L; // Long | idSection to delete or search
Long idRegion = 789L; // Long | idRegion to delete or search
Long idElection = 789L; // Long | idElection to delete or search
String deleted = "NOT-DELETED"; // String | Get all, deleted, not deleted data. Default not deleted.
Boolean metadata = true; // Boolean | If metadata is needed (for pagination controls)
try {
    Object result = apiInstance.getAccessBy(skip, limit, orderBy, idUser, idVotingTable, idInstitution, idCircuit, idSection, idRegion, idElection, deleted, metadata);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccessByApi#getAccessBy");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **skip** | **Integer**| number of item to skip |
 **limit** | **Integer**| max records to return |
 **orderBy** | **String**| order by property. | [optional]
 **idUser** | **Long**| id of user to get information only from it | [optional]
 **idVotingTable** | **Long**| idVotingTable to delete or search | [optional]
 **idInstitution** | **Long**| idInstitution to delete or search | [optional]
 **idCircuit** | **Long**| idCircuit to delete or search | [optional]
 **idSection** | **Long**| idSection to delete or search | [optional]
 **idRegion** | **Long**| idRegion to delete or search | [optional]
 **idElection** | **Long**| idElection to delete or search | [optional]
 **deleted** | **String**| Get all, deleted, not deleted data. Default not deleted. | [optional] [default to NOT-DELETED] [enum: NOT-DELETED, DELETED, ALL]
 **metadata** | **Boolean**| If metadata is needed (for pagination controls) | [optional]

### Return type

**Object**

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

<a name="getAccessByById"></a>
# **getAccessByById**
> AccessBy getAccessByById(idUser, idVotingTable, idInstitution, idCircuit, idSection, idRegion, idElection, deleted)

Get one AccessBy.

Get one AccessBy.

### Example
```java
// Import classes:
//import ar.com.singleton.fiscalizar.libs.java.ApiClient;
//import ar.com.singleton.fiscalizar.libs.java.ApiException;
//import ar.com.singleton.fiscalizar.libs.java.Configuration;
//import ar.com.singleton.fiscalizar.libs.java.auth.*;
//import ar.com.singleton.fiscalizar.libs.java.api.AccessByApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: appToken
ApiKeyAuth appToken = (ApiKeyAuth) defaultClient.getAuthentication("appToken");
appToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//appToken.setApiKeyPrefix("Token");

// Configure API key authorization: userToken
ApiKeyAuth userToken = (ApiKeyAuth) defaultClient.getAuthentication("userToken");
userToken.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//userToken.setApiKeyPrefix("Token");

AccessByApi apiInstance = new AccessByApi();
Long idUser = 789L; // Long | idUser to delete or search.
Long idVotingTable = 789L; // Long | idVotingTable to delete or search
Long idInstitution = 789L; // Long | idInstitution to delete or search
Long idCircuit = 789L; // Long | idCircuit to delete or search
Long idSection = 789L; // Long | idSection to delete or search
Long idRegion = 789L; // Long | idRegion to delete or search
Long idElection = 789L; // Long | idElection to delete or search
String deleted = "NOT-DELETED"; // String | Get all, deleted, not deleted data. Default not deleted.
try {
    AccessBy result = apiInstance.getAccessByById(idUser, idVotingTable, idInstitution, idCircuit, idSection, idRegion, idElection, deleted);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccessByApi#getAccessByById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idUser** | **Long**| idUser to delete or search. |
 **idVotingTable** | **Long**| idVotingTable to delete or search |
 **idInstitution** | **Long**| idInstitution to delete or search |
 **idCircuit** | **Long**| idCircuit to delete or search |
 **idSection** | **Long**| idSection to delete or search |
 **idRegion** | **Long**| idRegion to delete or search |
 **idElection** | **Long**| idElection to delete or search |
 **deleted** | **String**| Get all, deleted, not deleted data. Default not deleted. | [optional] [default to NOT-DELETED] [enum: NOT-DELETED, DELETED, ALL]

### Return type

[**AccessBy**](AccessBy.md)

### Authorization

[appToken](../README.md#appToken), [userToken](../README.md#userToken)

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

