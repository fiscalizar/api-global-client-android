
# VotesSections

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**section** | [**Sections**](Sections.md) |  |  [optional]
**categories** | [**List&lt;VotesCategories&gt;**](VotesCategories.md) |  |  [optional]
**circuits** | [**List&lt;VotesCircuits&gt;**](VotesCircuits.md) |  |  [optional]
**votedTables** | **Long** |  |  [optional]
**percentageByVotedTables** | **Integer** |  |  [optional]
**votedElectors** | **Long** |  |  [optional]
**percentageByVotedElectors** | **Integer** |  |  [optional]



