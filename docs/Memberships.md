
# Memberships

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**idUser** | **Long** |  |  [optional]
**user** | [**Users**](Users.md) |  |  [optional]
**idAccount** | **Long** |  |  [optional]
**account** | [**Accounts**](Accounts.md) |  |  [optional]
**role** | [**MembershipsRoles**](MembershipsRoles.md) |  |  [optional]



