
# LoginObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user** | [**Users**](Users.md) |  |  [optional]
**token** | **String** |  |  [optional]
**expDate** | [**Date**](Date.md) |  |  [optional]



