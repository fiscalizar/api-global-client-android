
# VotesCategories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | [**Categories**](Categories.md) |  |  [optional]
**parties** | [**List&lt;VotesParties&gt;**](VotesParties.md) |  |  [optional]
**votes** | **Long** |  |  [optional]



